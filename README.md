# Project IPTV CMS (Front End For Server Use ReactJs)

Use ReactJs Version 16.2.0 , if you want clone this repository use ReactJs version same.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

First you shoud be install Node Js then install NPM and you can get ReactJs.

To install Node Js 
```
pkg install nodejs
```
in this project use Node v8.9.1, dont use different version.
```
pkg install nodejs-8.9.1
```
if done install node, then you mush install NPM, in this project use npm version 5.5.1
```
npm install npm@5.5.1
```
if all done not showing error, you can get this repository run well.
### Installing

Make sure that git is already installed on the system. If you need help about this, you can serching "how to install git on {your sistem}" use Google. 

Open your terminal and change directory 
```
cd /media/abah/hardisk/projectlibrary/
```

And then you get clone
```
git clone https://wahyusetiaone@bitbucket.org/wahyusetiaone/front-end-iptv-cms-client.git
```

Until here, you has a repository in your local machine

## Running the tests

To running this project
```
npm start
```
then whil be show console log this project and this app start on port 8000

open your browser then access
```
http://localhost:8000/
```

## Built With

```
    "dependencies": {
        "@types/googlemaps": "^3.30.8",
        "@types/markerclustererplus": "^2.1.33",
        "ajv": "^6.0.0",
        "ajv-keywords": "^3.1.0",
        "bower": "^1.8.4",
        "caniuse-lite": "^1.0.30000823",
        "chartist": "^0.10.1",
        "classnames": "^2.2.5",
        "material-ui": "1.0.0-beta.34",
        "material-ui-icons": "1.0.0-beta.17",
        "npm-run-all": "^4.1.1",
        "perfect-scrollbar": "^1.3.0",
        "react": "^16.2.0",
        "react-chartist": "^0.13.1",
        "react-dom": "^16.2.0",
        "react-google-maps": "^9.4.5",
        "react-modal": "^3.3.2",
        "react-popup": "^0.9.2",
        "react-router-dom": "^4.2.2",
        "react-scripts": "^1.0.17",
        "react-swipeable-views": "^0.12.12"
		}
		
	"devDependencies": {
        "cross-env": "^5.1.4",
        "css-loader": "^0.28.11",
        "style-loader": "^0.20.3"
    }
```

## Versioning

This Repository Version Beta Tester 


