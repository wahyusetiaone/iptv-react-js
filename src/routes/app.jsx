import DashboardPage from "views/Dashboard/Dashboard.jsx";
// import UserProfile from "views/UserProfile/UserProfile.jsx";
import TableListSTB from "views/TableListSTB/TableList.jsx";
import TableListCh from "views/TableListCh/TableList.jsx";
// import Typography from "views/Typography/Typography.jsx";
// import Icons from "views/Icons/Icons.jsx";
import Maps from "views/Maps/Maps.jsx";
// import NotificationsPage from "views/Notifications/Notifications.jsx";
import Member from "views/Member/Member.jsx";

import {
  Dashboard,
  // Person,
  ContentPaste,
  LibraryBooks,
  // BubbleChart,
  LocationOn,
  // Notifications
} from "material-ui-icons";

const appRoutes = [
  {
    path: "/dashboard",
    sidebarName: "Dashboard",
    navbarName: "IPTV Dashboard",
    icon: Dashboard,
    component: DashboardPage
  },
  {
    path: "/member",
    sidebarName: "List Member",
    navbarName: "List Member",
    icon: LibraryBooks,
    component: Member
  },
  // {
  //   path: "/user",
  //   sidebarName: "User Profile",
  //   navbarName: "Profile",
  //   icon: Person,
  //   component: UserProfile
  // },
  {
    path: "/stb",
    sidebarName: "Set Box List",
    navbarName: "Set Box List",
    icon: ContentPaste,
    component: TableListSTB
  },
  {
    path: "/ch",
    sidebarName: "Channel List",
    navbarName: "Chanbel List",
    icon: ContentPaste,
    component: TableListCh
  },
  {
    path: "/maps",
    sidebarName: "Maps",
    navbarName: "Map",
    icon: LocationOn,
    component: Maps
  },
  // {
  //   path: "/notifications",
  //   sidebarName: "Notifications",
  //   navbarName: "Notifications",
  //   icon: Notifications,
  //   component: NotificationsPage
  // },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default appRoutes;
