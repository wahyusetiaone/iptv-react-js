import React from "react";
import { Grid } from "material-ui";
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import { RegularCard, Table, ItemGrid } from "components";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});


function TableSTBClient(props) {
  const { data } = props;
  return (
        <RegularCard
          plainCard
          cardTitle="Set Box Android Device"
          cardSubtitle="Free Device"
          content={
            <Table
              tableHeaderColor="primary"
              tableHead={["Nama Set Box", "SN", "Status"]}
              tableData={data}
            />
          }
        />
  );
}
TableSTBClient.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(TableSTBClient);
