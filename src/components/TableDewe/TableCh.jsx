import React from "react";
import { Grid } from "material-ui";
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import { RegularCard, Table, ItemGrid } from "components";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});

function TableCh(props) {
  const { data } = props;
  return (
    <Grid container>
      <ItemGrid xs={12} sm={12} md={12}>
        <RegularCard
          plainCard
          cardTitle="Table All Channel"
          cardSubtitle="All Channel"
          content={
            <Table
              tableHeaderColor="primary"
              tableHead={["ID", "Name", "Url", "Free"]}
              tableData={data}
            />
          }
        />
      </ItemGrid>
    </Grid>
  );
}
TableCh.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(TableCh);
