import React from "react";
import { Grid } from "material-ui";
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import { RegularCard, Table, ItemGrid } from "components";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});


function TableSTB(props) {
  const { data } = props;
  return (
    <Grid container>
      <ItemGrid xs={12} sm={12} md={12}>
        <RegularCard
          plainCard
          cardTitle="Set Box Android Device"
          cardSubtitle="List All Device"
          content={
            <Table
              tableHeaderColor="primary"
              tableHead={["SN", "Name", "Country", "City", "Status"]}
              tableData={data}
            />
          }
        />
      </ItemGrid>
    </Grid>
  );
}
TableSTB.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(TableSTB);
