import React from "react";
import { Grid } from "material-ui";
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import { RegularCard, Table, ItemGrid } from "components";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});


function TableChClient(props) {
  const { data } = props;
  return (
        <RegularCard
          plainCard
          cardTitle="Set Box Android Device"
          cardSubtitle="Free Device"
          content={
            <Table
              tableHeaderColor="primary"
              tableHead={["Nama Chanel", "Status"]}
              tableData={data}
            />
          }
        />
  );
}
TableChClient.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(TableChClient);
