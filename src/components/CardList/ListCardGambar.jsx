import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from "material-ui";
import { withStyles } from 'material-ui/styles';
import { RegularCard, ItemGrid } from "components";
import GridList, { GridListTile, GridListTileBar } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import InfoIcon from 'material-ui-icons/Info';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },

  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  button: {
    margin: theme.spacing.unit,
  }
});

function ListCardGambar({ ...props }) {
  const { classes,
    tileData,
    sendData } = props;


    //kirim balik nng kelas
  function pressTombol(event) {
    sendData(event);
  };

  return (
    <Grid container>
      <ItemGrid xs={12} sm={12} md={12}>
    <RegularCard
          plainCard
          cardTitle="Table All Channel"
          cardSubtitle="All Channel"
          content={
              <div className={classes.root}>
                <GridList cols= {3} cellHeight={220} className={classes.gridList}>
                  {tileData.map(tile => (
                    <GridListTile key={tile.owner}>
                      <img src={tile.img_url} alt={tile.nama_client} />
                      <GridListTileBar
                        title={tile.nama_client}
                        subtitle={<span>by: {tile.owner}</span>}
                        actionIcon={
                          <IconButton className={classes.icon} onClick={() =>pressTombol(tile)}>
                            <InfoIcon />
                          </IconButton>
                        }
                      />
                    </GridListTile>
                  ))}
                </GridList>
              </div>
          }
          />
          </ItemGrid>
          </Grid>
  );
}

ListCardGambar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListCardGambar);