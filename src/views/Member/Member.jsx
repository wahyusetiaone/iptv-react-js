import React from 'react';
import Modal from 'react-modal';
import AddIcon from 'material-ui-icons/Add';
import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';
import Button from 'material-ui/Button';
import { Grid } from "material-ui";

import {
  ListCardGambar,
  ItemGrid,
  RegularCard,
  CustomInput,
  ListChClient,
  ListSTBClient
} from "components";


const customStyles = {
  content : {
    top                   : '10%',
    left                  : '50%',
    bottom                : '20%'
  }
};

const customStyles2 = {
  content : {
    top                   : '10%',
    left                  : '20%',
    // right                 : '20%'
    bottom                : '5%'
    // marginRight           : '-50%',
    // transform             : 'translate(-50%, -50%)'
  }
};
 
const datacoba = [
  ["a","b","c"],
  ["a","b","c"],
  ["a","b","c"]
];
const datacoba2 = [
  ["a","b"],
  ["a","b"],
  ["a","b"]
];

class Member extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      dataMember : [],
      modalIsOpen : false,
      modalIsOpen2 : false,
      modalEdit:true,
      dataFromCom :{}
    }
    this.openModal = this.openModal.bind(this);
    this.openModal2 = this.openModal2.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.afterOpenModal2 = this.afterOpenModal2.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.closeModal2 = this.closeModal2.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  componentDidMount() {
    fetch('/users/partner',{
      method : 'POST',
      mode : 'CORS',
      body : JSON.stringify({id:1, data:"Get Member"}),
      headers : {
        'Content-Type':'application/json'
      } 
    }).then(response => response.json())
      .then(json => {
        console.log(json);
        this.setState({
          dataMember : json
        });
      }).catch(err => err);
  }

  componentWillMount(){
    this.setState(
      {key:this.state.dataFromCom.nama_client}
    )
  }
 
  openModal() {
    this.setState({modalIsOpen: true});
  }

  openModal2(val) {
    console.log(val);
    this.setState({modalIsOpen2: true});
    fetch('/users/details',{
      method : 'POST',
      mode : 'CORS',
      body : JSON.stringify({
              id:3, 
              data:"Get Details Member",
              username:val.user_client,
              password:val.pass_client
            }),
      headers : {
        'Content-Type':'application/json'
      } 
    }).then(response => response.json())
      .then(json => {
        console.log(json[0]);
        this.setState({
          dataFromCom : json[0]
        });
      }).catch(err => err);
  }
 
  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }

  afterOpenModal2() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }
 
  closeModal() {
    this.setState({modalIsOpen: false});
  }

  closeModal2() {
    this.setState({modalIsOpen2: false});
  }

  handleChange(event) {
    const fieldId = event.target.id;
    const fieldValue = event.target.value;
    var change = {}

    change[fieldId] = fieldValue;
    this.setState(change);
  }

  handleEdit(){
    if(this.state.modalEdit){
      this.setState({modalEdit : false});
    }else{
      this.handleSubmit("ChangeProfile");
      this.setState({modalEdit : true});
    }
  }

  handleSubmit(event) {
    if (event !== null && event === "ChangeProfile" && !this.state.modalEdit){
      fetch('/users/changeProfile',{
        method : 'post',
        mode : 'cors',
        body : JSON.stringify({id:3, data:"Change Profile", 
                id_client:this.state.dataFromCom.id_client,
                nama_client:this.state.nama_client, 
                user_client:this.state.user_client, 
                pass_client:this.state.pass_client, 
                owner:this.state.owner, 
                city:this.state.city, 
                country:this.state.country,
                postal_code:this.state.postal_code,
                email:this.state.email}),
        headers : {
          'Content-Type':'application/json'
        } 
      }).then(response => response.json())      
        .then(json => {
          console.log(json);
          if(json[0] === "Done"){
            this.componentDidMount();
            this.closeModal();
          }
        }).catch(err => err);
    }else{
      event.preventDefault();
      fetch('/users/add',{
        method : 'post',
        mode : 'cors',
        body : JSON.stringify({id:2, data:"Add Member", 
                username:this.state.username, 
                email:this.state.email, 
                hotel_name:this.state.hotel_name, 
                owner_name:this.state.owner_name, 
                city:this.state.city, 
                country:this.state.country,
                postal_code:this.state.postal_code}),
        headers : {
          'Content-Type':'application/json'
        } 
    }).then(response => response.json())      
        .then(json => {
          console.log(json);
          if(json[0] === "Done"){
            this.componentDidMount();
            this.closeModal();
          }
        }).catch(err => err);
    }
  }

  render() {
    var value = this.state.dataFromCom.nama_client;
  return (
      <div> 
          <ItemGrid xs={12} sm={12} md={12}>
              <ListCardGambar
              tileData = {this.state.dataMember}
              sendData={this.openModal2}
              >
          </ListCardGambar>
          <Button variant="fab" color="primary" aria-label="add" onClick={this.openModal}>
             <AddIcon />
           </Button>
          </ItemGrid>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
          ariaHideApp={false}
        >
 
          <ItemGrid xs={12} sm={12} md={12}>
          <RegularCard
            cardTitle="Add Profile"
            cardSubtitle="Complete your profile"
            content={
              <div>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Username"
                      id="username"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Email address"
                      id="email"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                </Grid>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Name Hotel"
                      id="hotel_name"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Owner Name"
                      id="owner_name"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                </Grid>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="City"
                      id="city"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Country"
                      id="country"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Postal Code"
                      id="postal_code"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                </Grid>
              </div>
            }
            footer={<Button variant="raised" color="secondary" onClick={this.handleSubmit}>Simpan</Button>}
          />
          
        </ItemGrid>
         

        </Modal>

        <Modal
          isOpen={this.state.modalIsOpen2}
          onAfterOpen={this.afterOpenModal2}
          onRequestClose={this.closeModal2}
          style={customStyles2}
          contentLabel="Example Modal"
          ariaHideApp={false}
        >
        <ItemGrid xs={12} sm={12} md={12}>
          <RegularCard
            cardTitle="Profile"
            cardSubtitle="This is Your Profile"
            content={
              <div>

                <Grid container>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Nama"
                      id="nama_client"
                      formControlProps={{
                        fullWidth: true
                      }}
                      onChange={this.handleChange}
                      // defaultValue={this.key}
                      placeholder={this.state.dataFromCom.nama_client}
                      inputProps={{
                        disabled: this.state.modalEdit
                      }}
                    />
                    <CustomInput
                      labelText="Username"
                      id="user_client"
                      formControlProps={{
                        fullWidth: true
                      }}
                      placeholder={this.state.dataFromCom.user_client}
                      onChange={this.handleChange}
                      inputProps={{
                        disabled: this.state.modalEdit
                      }}
                    />
                    <CustomInput
                      labelText="Password"
                      id="pass_client"
                      formControlProps={{
                        fullWidth: true
                      }}
                      placeholder={this.state.dataFromCom.pass_client}
                      onChange={this.handleChange}
                      inputProps={{
                        disabled: this.state.modalEdit
                      }}
                    />
                    <CustomInput
                      labelText="Owner"
                      id="owner"
                      formControlProps={{
                        fullWidth: true
                      }}
                      placeholder={this.state.dataFromCom.owner}
                      onChange={this.handleChange}
                      inputProps={{
                        disabled: this.state.modalEdit
                      }}
                    />
                    <CustomInput
                      labelText="City"
                      id="city"
                      formControlProps={{
                        fullWidth: true
                      }}
                      placeholder={this.state.dataFromCom.city}
                      onChange={this.handleChange}
                      inputProps={{
                        disabled: this.state.modalEdit
                      }}
                    />
                    <CustomInput
                      labelText="Country"
                      id="country"
                      formControlProps={{
                        fullWidth: true
                      }}
                      placeholder={this.state.dataFromCom.country}
                      onChange={this.handleChange}
                      inputProps={{
                        disabled: this.state.modalEdit
                      }}
                    />
                    <CustomInput
                      labelText="Postal"
                      id="postal_code"
                      formControlProps={{
                        fullWidth: true
                      }}
                      placeholder={this.state.dataFromCom.postal_code}
                      onChange={this.handleChange}
                      inputProps={{
                        disabled: this.state.modalEdit
                      }}
                    />
                    <CustomInput
                      labelText="Email address"
                      id="email"
                      formControlProps={{
                        fullWidth: true
                      }}
                      placeholder={this.state.dataFromCom.email}
                      onChange={this.handleChange}
                      inputProps={{
                        disabled: this.state.modalEdit
                      }}
                    />
                    <Button variant="fab" color="secondary" onClick={this.handleEdit}>
                    {this.state.modalEdit ? (
                      <EditIcon />
                    ) : (
                      <SaveIcon />
                    )}
                   </Button>  
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <ListSTBClient 
                        data = {datacoba}>
                    </ListSTBClient>
                    <ListChClient 
                        data = {datacoba2}>
                    </ListChClient>
                  </ItemGrid>
                </Grid>

              </div>
            }
            />
        </ItemGrid>
        </Modal>
      </div>
    );

  }
}


// Member.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

export default Member;