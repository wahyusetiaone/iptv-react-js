import React from "react";
import Modal from 'react-modal';
import AddIcon from 'material-ui-icons/Add';
import Button from 'material-ui/Button';
import { Grid } from "material-ui";
// import PropTypes from 'prop-types';
import {
  ListSTB,
  ListSTBFree,
  ItemGrid,
  RegularCard,
  CustomInput
} from "components";

const customStyles = {
  content : {
    top                   : '10%',
    left                  : '50%',
    // right                 : '20%'
    bottom                : '20%'
    // marginRight           : '-50%',
    // transform             : 'translate(-50%, -50%)'
  }
};

class TableList extends React.Component{
  constructor(props) {
    super(props);
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  state = {
    data : [],
    datafree :[],
    modalIsOpen : false
  }

  componentDidMount() {
    fetch('/lists/stb',{
      method : 'POST',
      mode : 'CORS',
      body : JSON.stringify({id:1, data:"Get STB"}),
      headers : {
        'Content-Type':'application/json'
      } 
    }).then(response => response.json())
      .then(json => {
        console.log(json);
        this.setState({
          data : json.data,
          datafree :json.dataall
        });
      }).catch(err => err);
  } 

  openModal() {
    this.setState({modalIsOpen: true});
  }
 
  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }
 
  closeModal() {
    this.setState({modalIsOpen: false});
  }

  handleChange(event) {
    const fieldId = event.target.id;
    const fieldValue = event.target.value;
    var change = {}

    change[fieldId] = fieldValue;
    this.setState(change);
  }

  handleSubmit(event) {
    event.preventDefault();
      fetch('/lists/add',{
        method : 'post',
        mode : 'cors',
        body : JSON.stringify({id:2, data:"Add STB", 
                serial_number:this.state.serial_number, 
                name:this.state.name}),
        headers : {
          'Content-Type':'application/json'
        } 
    }).then(response => response.json())      
        .then(json => {
          console.log(json);
          if(json[0] === "Done"){
            this.componentDidMount();
            this.closeModal();
          }
        }).catch(err => err);
  }

  render() {
    
  return (
      <div> 
         <Grid container>
            <ItemGrid xs={8} sm={8} md={8}>
              <ListSTB 
              data = {this.state.data}>
              </ListSTB>
            </ItemGrid>
            <ItemGrid xs={3} sm={3} md={3}>
              <ListSTBFree 
              data = {this.state.datafree}>
              </ListSTBFree>
            </ItemGrid>
            <ItemGrid xs={12} sm={12} md={12}>
              <Button variant="fab" color="primary" aria-label="add" onClick={this.openModal}>
                <AddIcon />
              </Button>
            </ItemGrid>
          </Grid>
            <Modal
              isOpen={this.state.modalIsOpen}
              onAfterOpen={this.afterOpenModal}
              onRequestClose={this.closeModal}
              style={customStyles}
              contentLabel="Example Modal"
              ariaHideApp={false}
            >
    
              <ItemGrid xs={12} sm={12} md={12}>
              <RegularCard
                cardTitle="Add Set Box Tv"
                cardSubtitle="Becareful to insert Serial Number"
                content={
                  <div>
                    <Grid container>
                      <ItemGrid xs={12} sm={12} md={6}>
                        <CustomInput
                          labelText="Serial Number"
                          id="serial_number"
                          onChange={this.handleChange}
                          formControlProps={{
                            fullWidth: true
                          }}
                        />
                      </ItemGrid>
                      <ItemGrid xs={12} sm={12} md={6}>
                        <CustomInput
                          labelText="Name"
                          id="name"
                          onChange={this.handleChange}
                          formControlProps={{
                            fullWidth: true
                          }}
                        />
                      </ItemGrid>
                    </Grid>
                    <Grid container>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="Country (Disable)"
                          id="country"
                          onChange={this.handleChange}
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            disabled: true
                          }}
                        />
                      </ItemGrid>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="City (Disable)"
                          id="city"
                          onChange={this.handleChange}
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            disabled: true
                          }}
                        />
                      </ItemGrid>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="Status (Disable)"
                          id="status"
                          onChange={this.handleChange}
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            disabled: true
                          }}
                        />
                      </ItemGrid>
                    </Grid>
                  </div>
                }
                footer={<Button variant="raised" color="secondary" onClick={this.handleSubmit}>Simpan</Button>}
              />
              
            </ItemGrid>
            </Modal>
      </div>
    );

  }
}

// TableList.propTypes = {
//   classes: PropTypes.object.isRequired,
// };
export default TableList;
