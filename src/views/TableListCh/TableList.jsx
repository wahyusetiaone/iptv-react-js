import React from "react";
import Modal from 'react-modal';
import AddIcon from 'material-ui-icons/Add';
import Button from 'material-ui/Button';
// import DropDownMenu from 'material-ui/DropDownMenu';
// import MenuItem from 'material-ui/MenuItem';
import { Grid } from "material-ui";

// import PropTypes from 'prop-types';
import {
  ListCh,
  ItemGrid,
  RegularCard,
  CustomInput
} from "components";

const customStyles = {
  content : {
    top                   : '10%',
    left                  : '50%',
    // right                 : '20%'
    bottom                : '30%'
    // marginRight           : '-50%',
    // transform             : 'translate(-50%, -50%)'
  }
};

// const items = [];
// for (let i = 0; i < 100; i++ ) {
//   items.push(<MenuItem value={i} key={i} primaryText={`Item ${i}`} />);
// }

class TableList extends React.Component{
  constructor(props) {
    super(props);
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  state = {
    dataList : [],
    modalIsOpen : false
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }
 
  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }
 
  closeModal() {
    this.setState({modalIsOpen: false});
  }

  componentDidMount(){
    fetch('/channel/ch',{
      method : 'POST',
      mode : 'CORS',
      body : JSON.stringify({id:1,data:"Get Ch"}),
      headers : {
        'Content-Type':'application/json'
      }
    }).then(response => response.json())
      .then(json => {
        console.log(json);
        this.setState({
          dataList : json
        });
      }).catch(err => err);
  }

  handleChange(event) {
    const fieldId = event.target.id;
    const fieldValue = event.target.value;
    var change = {}

    change[fieldId] = fieldValue;
    this.setState(change);
  }

  handleSubmit(event) {
    event.preventDefault();
      fetch('/channel/add',{
        method : 'post',
        mode : 'cors',
        body : JSON.stringify({id:2, data:"Add Channel",
                channel_name:this.state.channel_name, 
                channel_url:this.state.channel_url, 
                free:this.state.free,
                img:this.state.img}),
        headers : {
          'Content-Type':'application/json'
        } 
    }).then(response => response.json())      
        .then(json => {
          console.log(json);
          if(json[0] === "Done"){
            this.componentDidMount();
            this.closeModal();
          }
        }).catch(err => err);
  }

  render(){
    return(
      <div>
        <ItemGrid xs={12} sm={12} md={12}>
          <ListCh 
          data = {this.state.dataList}>
          </ListCh>
        </ItemGrid>
        <Button variant="fab" color="primary" aria-label="add" onClick={this.openModal}>
             <AddIcon />
        </Button>

        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
          ariaHideApp={false}
        >
 
          <ItemGrid xs={12} sm={12} md={12}>
          <RegularCard
            cardTitle="Add Channel"
            cardSubtitle="Please insert fix url !!!"
            content={
              <div>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="ID Channel (disable)"
                      id="channel_id"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        disabled: true
                      }}
                    />
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Name Channel"
                      id="channel_name"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                </Grid>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Url Channel"
                      id="channel_url"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Free (yes/no)"
                      id="free"
                      onChange={this.handleChange}
                      formControlProps={{
                        fullWidth: true
                      }}
                    />
                  </ItemGrid>
                </Grid>
                {/* <Grid container>
                  <ItemGrid xs={12} sm={12} ms={4}>
                  <DropDownMenu maxHeight={300} value={this.state.value} onChange={this.handleChange}>
                    {items}
                  </DropDownMenu>
                  </ItemGrid>
                </Grid> */}
              </div>
            }
            footer={<Button variant="raised" color="secondary" onClick={this.handleSubmit}>Simpan</Button>}
          />
          
        </ItemGrid>
         

        </Modal>
      </div>
    );
  }
}

// TableList.propTypes = {
//   classes: PropTypes.object.isRequired,
// };
export default TableList;
